const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const customerRouter = require("./routes/customerRoute");
const genreRouter = require("./routes/genreRoute");
const movieRouter = require("./routes/movieRoute");
const rentalRouter = require("./routes/rentalRoute");

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/movie-rental", { useNewUrlParser: true });

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(customerRouter);
app.use(genreRouter);
app.use(movieRouter);
app.use(rentalRouter);
app.use((err, req, res, next) => {
  res.status(400).send(err.message);
});

const port = 3000;
app.listen(port, () => {
  console.log(`Listening at port : ${port}`);
});
