const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const genschema = new Schema({
  name: {
    type: String,
    required: [true, "Please enter the genre"],
    minlength: [1, "Too few characters"]
  }
});

const genreModel = mongoose.model("genres", genschema);
module.exports = genreModel;
