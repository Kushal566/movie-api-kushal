const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const rentalSchema = new Schema({
  customer: {
    type: Schema.Types.ObjectId,
    ref: "customers",
    required: true
  },
  movie: {
    type: Schema.Types.ObjectId,
    ref: "movies",
    required: true
  },
  dateIssued: {
    type: Date,
    required: true
  },
  dateReturned: {
    type: Date,
    required: true
  },
  rentalFee: {
    type: Number,
    required: true
  }
});

const rentalModel = mongoose.model("rental", rentalSchema);
module.exports = rentalModel;
