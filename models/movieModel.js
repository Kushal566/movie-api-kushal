const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const dbschema = new Schema({
  title: {
    type: String,
    required: [true, "Title is required"],
    minlength: [1, "Too few characters"]
  },
  genre: {
    type: String,
    required: [true, "Genre is required"],
    minlength: [3, "Too few characters"]
  },
  numberInStock: {
    type: Number,
    required: [true, "Enter no of current Stock"]
  },
  dailyRentalRate: {
    type: Number,
    required: [true, "Enter the Rental Rate"]
  }
});

const movieModel = mongoose.model("movies", dbschema);

module.exports = movieModel;
