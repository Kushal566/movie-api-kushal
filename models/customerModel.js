const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const customerSchema = new Schema({
  name: {
    type: String,
    required: true,
    minlength: [2, "Too few characters"]
  },
  isPremium: {
    type: Boolean,
    required: true
  },
  phone: {
    type: String,
    required: true,
    minlength: [10, "Too few characters"],
    maxlength: [10, "Too many characters"]
  }
});

const customerModel = mongoose.model("customers", customerSchema);
module.exports = customerModel;
