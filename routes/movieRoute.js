const express = require("express");
const movieModel = require("../models/movieModel");

const movieRouter = express.Router();

movieRouter.get("/api/movies", (req, res, next) => {
  movieModel
    .find({}, (err, movies) => {
      res.send(movies);
    })
    .catch(next);
});

movieRouter.post("/api/movies", (req, res, next) => {
  movieModel
    .create(req.body)
    .then(newMovie => {
      res.send(newMovie);
    })
    .catch(next);
});

movieRouter.get("/api/movies/:movieid", (req, res) => {
  movieModel.find({ _id: req.params.movieid }, (err, movie) => {
    if (err) {
      res.status(400).send(err.message);
      return;
    }
    res.send(movie);
  });
});

movieRouter.put("/api/movies/:movieid", (req, res, next) => {
  movieModel
    .findByIdAndUpdate({ _id: req.params.movieid }, req.body)
    .then(() => {
      movieModel.find({ _id: req.params.movieid }, (err, movie) => {
        res.send(movie);
      });
    })
    .catch(next);
});

movieRouter.delete("/api/movies/:movieid", (req, res) => {
  movieModel
    .findByIdAndRemove({ _id: req.params.movieid })
    .then(() => {
      res.send("Movie Deleted");
    })
    .catch(res.status(400).send("Movie Id not found!!!!"));
});

module.exports = movieRouter;