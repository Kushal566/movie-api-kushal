const express = require("express");
const rentalModel = require("../models/rentalModel");

const rentalRouter = express.Router();

rentalRouter.get("/api/rentals", (req, res) => {
    rentalModel
      .find({})
      .populate("customer movie")
      .exec((err, customer) => {
        res.send(customer);
      });
  });
  
  rentalRouter.post("/api/rentals", (req, res, next) => {
    rentalModel
      .create(req.body)
      .then(newRental => {
        res.send(newRental);
      })
      .catch(next);
  });

  module.exports = rentalRouter;
