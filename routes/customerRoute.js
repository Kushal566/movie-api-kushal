const express = require("express");
const customerModel = require("../models/customerModel");

const customerRouter = express.Router();

customerRouter.get("/api/customers", (req, res) => {
  customerModel
    .find({}, (err, genres) => {
      if (err) {
        res.status(400).send(err.message);
        return;
      }
      res.send(genres);
    })
    .catch("Could not retreive data");
});

customerRouter.post("/api/customers", (req, res, next) => {
  customerModel
    .create(req.body)
    .then(newCustomer => {
      res.send(newCustomer);
    })
    .catch(next);
});

customerRouter.get("/api/customers/:customerid", (req, res, next) => {
  customerModel
    .find({ _id: req.params.customerid }, (err, customer) => {
      res.send(customer);
    })
    .catch(next);
});

customerRouter.put("/api/customers/:customerid", (req, res, next) => {
  customerModel
    .findOneAndUpdate({ _id: req.params.customerid }, req.body)
    .then(() => {
      customerModel.find({ _id: req.params.customerid }).then(customer => {
        res.send(customer);
      });
    })
    .catch(next);
});

customerRouter.delete("/api/customers/:customerid", (req, res, next) => {
  customerModel
    .findByIdAndRemove({ _id: req.params.customerid }, req.body)
    .then(() => {
      res.send("Customer data deleted");
    })
    .catch(next);
});

module.exports = customerRouter;
