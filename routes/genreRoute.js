const express = require("express");
const genreModel = require("../models/genreModel");

const genreRouter = express.Router();

genreRouter.get("/api/genres", (req, res, next) => {
  genreModel
    .find({}, (err, genres) => {
      res.send(genres);
    })
    .catch(next);
});

genreRouter.post("/api/genres", (req, res, next) => {
  genreModel
    .create(req.body)
    .then(newGenre => {
      res.send(newGenre);
    })
    .catch(next);
});

genreRouter.get("/api/genres/:genreid", (req, res, next) => {
  genreModel
    .find({ _id: req.params.genreid }, (err, genre) => {
      if (err) {
        res.status(400).send(err.message);
        return;
      }
      res.send(genre);
    })
    .catch(next);
});

genreRouter.put("/api/genres/:genreid", (req, res, next) => {
  genreModel
    .findByIdAndUpdate({ _id: req.params.genreid }, req.body)
    .then(() => {
      genreModel.find({ _id: req.params.genreid }).then(genre => {
        res.send(genre);
      });
    })
    .catch(next);
});

genreRouter.delete("/api/genres/:genreid", (req, res, next) => {
  genreModel
    .findByIdAndRemove({ _id: req.params.genreid })
    .then(() => {
      res.send("Genre Deleted");
    })
    .catch(next);
});

module.exports = genreRouter;
